Name:       i2c-tools
Summary:    Heterogeneous set of I2C tools for Linux
Version:    4.1
Release:    1
Group:      System/Libraries
License:    GPLv2+
Source0:    http://dl.lm-sensors.org/i2c-tools/releases/%{name}-%{version}.tar.bz2

%description
This package contains a heterogeneous set of I2C tools for Linux: a bus
probing tool, a chip dumper, register-level access helpers, EEPROM
decoding scripts, and more.

%package -n libi2c-devel
Summary:    Userspace I2C programming library development files
Group:      System/Libraries
Requires:   %{name} = %{version}-%{release}

%description -n libi2c-devel
I2C devices are usually controlled by a kernel driver. Using this
library it is also possible to access all devices on an adapter
from userspace and without the knowledge of Linux kernel internals.

%prep
%setup -q -n %{name}-%{version}/upstream

%build
make %{?jobs:-j%jobs}

%install
rm -rf %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT PREFIX=%{_prefix}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/ddcmon
%{_bindir}/decode-dimms
%{_bindir}/decode-edid
%{_bindir}/decode-vaio
%{_sbindir}/i2c-stub-from-dump
%{_sbindir}/i2cdetect
%{_sbindir}/i2cdump
%{_sbindir}/i2cget
%{_sbindir}/i2cset
%{_sbindir}/i2ctransfer
%{_libdir}/libi2c.so.*
%{_mandir}/man8/*

%files -n libi2c-devel
%defattr(-,root,root,-)
%{_includedir}/i2c/*.h
%{_libdir}/libi2c.so
%{_libdir}/libi2c.a
%{_mandir}/man1/*
